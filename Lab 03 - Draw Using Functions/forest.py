import math

import arcade
arcade.open_window(600,600,"Drawing Example")
arcade.set_background_color(arcade.csscolor.DEEP_SKY_BLUE)
arcade.start_render()

arcade.draw_lrtb_rectangle_filled(0,600, 200, 0, arcade.csscolor.LIME_GREEN)

i=1
while 90 + 10 * i < 600 :
    y = math.sqrt(300 * 300 - (300-0 * i)* (300-0*i)) +180
    arcade.draw_rectangle_filled( 5 + 90* i, y ,20,50,arcade.color.SIENNA)
    i += 1

j=1
while 90 + 10 * j < 600 :
    y = math.sqrt(300 * 300 - (300-0 * j)* (300-0*j)) +180
    arcade.draw_triangle_filled(5+ 90 * j,300,-35 +90 * j, y+20,45+ 90 * j,200 ,arcade.color.DARK_GREEN)
    j += 1

    i = 1
    while 90 + 10 * i < 600:
        y = math.sqrt(300 * 300 - (300 - 0 * i) * (300 - 0 * i)) + 50
        arcade.draw_rectangle_filled(5 + 70 * i, y, 20, 50, arcade.color.SIENNA)
        i += 1

    i = 1
    while 90 + 10 * i < 600:
        y = math.sqrt(300 * 300 - (300 - 0 * i) * (300 - 0 * i)) + 50
        arcade.draw_triangle_filled(5 +70 * i,150, -30 + 70 * i, y + 20, 40 + 70 * i, 70, arcade.color.DARK_GREEN)
        i += 1

arcade.draw_circle_filled(82,400,10, arcade.color.BLACK)
arcade.draw_circle_filled(81, 399,10, arcade.color.DEEP_SKY_BLUE)
arcade.draw_circle_filled(100, 400,10, arcade.color.BLACK)
arcade.draw_circle_filled(101, 399, 10, arcade.color.DEEP_SKY_BLUE)

arcade.draw_circle_filled(81,400,10, arcade.color.BLACK)
arcade.draw_circle_filled(80, 399,10, arcade.color.DEEP_SKY_BLUE)
arcade.draw_circle_filled(100, 400,10, arcade.color.BLACK)
arcade.draw_circle_filled(101, 399, 10, arcade.color.DEEP_SKY_BLUE)

arcade.draw_circle_filled(250,500,10, arcade.color.BLACK)
arcade.draw_circle_filled(249, 499,10, arcade.color.DEEP_SKY_BLUE)
arcade.draw_circle_filled(269, 500,10, arcade.color.BLACK)
arcade.draw_circle_filled(270, 499, 10, arcade.color.DEEP_SKY_BLUE)

arcade.draw_circle_filled(400,300,10, arcade.color.BLACK)
arcade.draw_circle_filled(399, 299,10, arcade.color.DEEP_SKY_BLUE)
arcade.draw_circle_filled(419, 300,10, arcade.color.BLACK)
arcade.draw_circle_filled(420, 299, 10, arcade.color.DEEP_SKY_BLUE)

arcade.draw_circle_filled(530,530,10, arcade.color.BLACK)
arcade.draw_circle_filled(529, 529,10, arcade.color.DEEP_SKY_BLUE)
arcade.draw_circle_filled(549, 530,10, arcade.color.BLACK)
arcade.draw_circle_filled(550, 529, 10, arcade.color.DEEP_SKY_BLUE)

arcade.finish_render()

arcade.run()