import arcade

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


def draw_grass():
    """绘制地面"""
    arcade.draw_lrtb_rectangle_filled(0, SCREEN_WIDTH, SCREEN_HEIGHT / 3, 0, arcade.color.AIR_SUPERIORITY_BLUE)


def draw_snow_man(x, y):
    """绘制雪人"""

    # 雪人身体
    arcade.draw_circle_filled(x, 60 + y, 60, arcade.color.WHITE)
    arcade.draw_circle_filled(x, 140 + y, 50, arcade.color.WHITE)
    arcade.draw_circle_filled(x, 200 + y, 40, arcade.color.WHITE)

    # 眼睛
    arcade.draw_circle_filled(x - 15, 210 + y, 5, arcade.color.BLACK)
    arcade.draw_circle_filled(x + 15, 210 + y, 5, arcade.color.BLACK)


def on_draw(delta_time):
    """绘制全部内容"""
    arcade.start_render()
    draw_grass()
    draw_snow_man(on_draw.start_x, 140)
    draw_snow_man(450, 180)

    # 每调用一次on_draw, start_x + 1
    on_draw.start_x += 1


# 定义雪人x轴位置变量
on_draw.start_x = 150


def main():
    arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "函数画图")
    arcade.set_background_color(arcade.color.DARK_BLUE)

    # 每1/60秒调用一次on_draw函数
    arcade.schedule(on_draw, 1 / 60)
    arcade.run()


if __name__ == "__main__":
    # 调用main()函数
    main()
