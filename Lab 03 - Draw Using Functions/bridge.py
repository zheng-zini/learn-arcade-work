import math

import arcade
arcade.open_window(600,600,"Drawing Example")
arcade.set_background_color(arcade.csscolor.CORAL)
arcade.start_render()

arcade.draw_lrtb_rectangle_filled(0, 600,90, 0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(0,600,160,90, arcade.csscolor.NAVAJO_WHITE)
arcade.draw_lrtb_rectangle_filled(0,600,230,160, arcade.csscolor.BISQUE)
arcade.draw_lrtb_rectangle_filled(0,600,290,230, arcade.csscolor.DARK_SALMON)
arcade.draw_lrtb_rectangle_filled(0,600,350,290, arcade.csscolor.LIGHT_SALMON)

arcade.draw_circle_filled(450, 520, 1, arcade.color.WHITE)
arcade.draw_circle_filled(430, 510, 2, arcade.color.WHITE)
arcade.draw_circle_filled(550, 430, 2, arcade.color.WHITE)
arcade.draw_circle_filled(460, 495, 1, arcade.color.WHITE)
arcade.draw_circle_filled(525, 433, 1, arcade.color.WHITE)
arcade.draw_circle_filled(450, 395, 1, arcade.color.WHITE)
arcade.draw_circle_filled(399, 500, 1, arcade.color.WHITE)
arcade.draw_circle_filled(5, 550, 1, arcade.color.WHITE)
arcade.draw_circle_filled(95, 499, 1, arcade.color.WHITE)
arcade.draw_circle_filled(118, 418, 1, arcade.color.WHITE)
arcade.draw_circle_filled(77, 555, 2, arcade.color.WHITE)
arcade.draw_circle_filled(56, 444, 1, arcade.color.WHITE)
arcade.draw_circle_filled(222, 530, 1, arcade.color.WHITE)
arcade.draw_circle_filled(252, 466, 1, arcade.color.WHITE)
arcade.draw_circle_filled(225, 460, 1, arcade.color.WHITE)
arcade.draw_circle_filled(325, 475, 1, arcade.color.WHITE)
arcade.draw_circle_filled(155, 385, 2, arcade.color.WHITE)
arcade.draw_circle_filled(500, 570, 2, arcade.color.WHITE)
arcade.draw_circle_filled(80, 400, 1, arcade.color.WHITE)
arcade.draw_circle_filled(225, 460, 1, arcade.color.WHITE)
arcade.draw_circle_filled(150, 530, 2, arcade.color.WHITE)
arcade.draw_circle_filled(512, 500, 2, arcade.color.WHITE)
arcade.draw_circle_filled(355, 550, 1, arcade.color.WHITE)
arcade.draw_circle_filled(315, 480, 1, arcade.color.WHITE)

arcade.draw_lrtb_rectangle_filled(0,10,140,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(10,20,120,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(22,27,148,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(29,40,115,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(40,45,130,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(46,53,165,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(53,58,144,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(60,67,122,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(67,73,133,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(74,79,100,0, arcade.csscolor.BLACK)

arcade.draw_lrtb_rectangle_filled(532,539,140,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(539,544,120,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(546,551,148,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(553,563,115,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(563,568,130,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(569,576,165,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(576,581,144,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(583,590,122,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(590,595,133,0, arcade.csscolor.BLACK)
arcade.draw_lrtb_rectangle_filled(595,600,100,0, arcade.csscolor.BLACK)

arcade.draw_circle_filled(85,250,8, arcade.color.BLACK)
arcade.draw_circle_filled(85, 248, 8, arcade.color.DARK_SALMON)
arcade.draw_circle_filled(98, 250,8, arcade.color.BLACK)
arcade.draw_circle_filled(98, 248, 8, arcade.color.DARK_SALMON)

arcade.draw_circle_filled(120,255,7, arcade.color.BLACK)
arcade.draw_circle_filled(120, 253, 7, arcade.color.DARK_SALMON)
arcade.draw_circle_filled(133, 255,7, arcade.color.BLACK)
arcade.draw_circle_filled(133, 253, 7, arcade.color.DARK_SALMON)

arcade.draw_circle_filled(82,270,5, arcade.color.BLACK)
arcade.draw_circle_filled(82, 269,5, arcade.color.DARK_SALMON)
arcade.draw_circle_filled(90, 270,5, arcade.color.BLACK)
arcade.draw_circle_filled(90, 269, 5, arcade.color.DARK_SALMON)

arcade.draw_circle_filled(485,215,9, arcade.color.BLACK)
arcade.draw_circle_filled(485, 213, 9, arcade.color.BISQUE)
arcade.draw_circle_filled(503, 215,9, arcade.color.BLACK)
arcade.draw_circle_filled(503, 213, 9, arcade.color.BISQUE)

arcade.draw_circle_filled(497,200,7, arcade.color.BLACK)
arcade.draw_circle_filled(497, 198, 7, arcade.color.BISQUE)
arcade.draw_circle_filled(510, 200,7, arcade.color.BLACK)
arcade.draw_circle_filled(510, 198, 7, arcade.color.BISQUE)

arcade.draw_circle_filled(530,210,5, arcade.color.BLACK)
arcade.draw_circle_filled(530, 209, 5, arcade.color.BISQUE)
arcade.draw_circle_filled(538, 210,5, arcade.color.BLACK)
arcade.draw_circle_filled(538, 209, 5, arcade.color.BISQUE)

arcade.draw_line(50,20,550,20, arcade.color.BLACK)
arcade.draw_line(50 , 20,550 , 20, arcade.color.BLACK)

arcade.draw_arc_outline (center_x=300, center_y=-130, width=430, height= 600,color= arcade.csscolor.BLACK, start_angle= 45, end_angle=135, border_width= 5, tilt_angle= 0, num_segments = 128)
arcade.draw_arc_outline (center_x=300, center_y=-90, width=500, height= 600,color= arcade.csscolor.BLACK, start_angle= 25, end_angle=155, border_width= 15, tilt_angle= 0, num_segments =128)
i=1
while 90 + 10 * i < 490 :
    y = math.sqrt(210 * 210 - (210 -10 * i)* (210-10*i)) + 4
    arcade.draw_line(90+ 10 * i,20, 90 + 10 * i, y ,arcade.color.BLACK)
    i += 1

    

arcade.finish_render()

arcade.run()