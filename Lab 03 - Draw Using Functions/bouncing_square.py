import arcade
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "函数画图")
arcade.set_background_color(arcade.csscolor.MEDIUM_PURPLE)

def draw_rectangle(x,y):
 arcade.draw_rectangle_filled(x, 10+y, 40, 40, arcade.csscolor.BLUE_VIOLET)



def on_draw(delta_time):
    """绘制全部内容"""
    arcade.start_render()
    draw_rectangle(on_draw.start_x,on_draw.start_y)

    # 每调用一次on_draw, start_x + 1
    on_draw.start_x += 1
    on_draw.start_y += 1
    on_draw.end_x -=1
    on_draw.end_x -=1

# 定义雪人x轴位置变量
on_draw.start_x = 0
on_draw.start_y = 0
on_draw.end_x = SCREEN_WIDTH
on_draw.end_y = SCREEN_HEIGHT
def main():


    # 每1/60秒调用一次on_draw函数
    arcade.schedule(on_draw, 1/200)
    arcade.run()


if __name__ == "__main__":
    # 调用main()函数
    main()
